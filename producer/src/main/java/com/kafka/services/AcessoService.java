package com.kafka.services;

import com.kafka.Acesso;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class AcessoService {

        @Autowired
        private KafkaTemplate<String, Acesso> producer;

        public void enviarAoKafka(Acesso acesso) {
            producer.send("spec3-rafael-antonio-1", acesso);
        }

}
