package com.kafka.controllers;

import com.kafka.Acesso;
import com.kafka.services.AcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/acesso")
public class AcessoController {

    @Autowired
    AcessoService acessoService;

    @GetMapping("{cliente}/{porta}")
    public Acesso validarAcesso(@PathVariable(value="cliente") int idcliente, @PathVariable(value="porta") int idporta)
    {

        Acesso objAcesso = new Acesso();

        objAcesso.setIdAcesso(1);
        objAcesso.setAcessoAtivo(true);

        objAcesso.setIdCliente(idcliente);
        objAcesso.setIdPorta(idporta);

        acessoService.enviarAoKafka(objAcesso);

        return objAcesso;


    }

}
