package com.kafka;

import com.opencsv.CSVWriter;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;
import sun.rmi.runtime.Log;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component
public class AcessoConsumer {

    @KafkaListener(topics = "spec3-rafael-antonio-1", groupId = "seguranca")
    public void receber(@Payload Acesso acesso) throws IOException {

        System.out.println("Acesso - " + acesso.getIdCliente());

        FileWriter csvFileWriter = new FileWriter("log.csv", true);
        
        csvFileWriter.append("Acesso - " + acesso.getIdCliente());
        csvFileWriter.append("\n");

        csvFileWriter.flush();
        csvFileWriter.close();


    }

}
