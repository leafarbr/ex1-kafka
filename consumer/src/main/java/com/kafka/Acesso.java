package com.kafka;

public class Acesso {

    int idAcesso;
    int idCliente;
    int idPorta;
    boolean acessoAtivo;

    public int getIdAcesso() {
        return idAcesso;
    }

    public void setIdAcesso(int idAcesso) {
        this.idAcesso = idAcesso;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public int getIdPorta() {
        return idPorta;
    }

    public void setIdPorta(int idPorta) {
        this.idPorta = idPorta;
    }

    public boolean isAcessoAtivo() {
        return acessoAtivo;
    }

    public void setAcessoAtivo(boolean acessoAtivo) {
        this.acessoAtivo = acessoAtivo;
    }

    public Acesso() {
    }

}
